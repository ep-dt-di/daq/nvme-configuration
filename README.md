
# NVMe Drives Configuration

Ansible Playbook to install and setup RAID between NVMe drives.

1. Setup the host file

2. Setup raid_configuration file in /roles/nvme/vars and enter your user name for ssh connection

3. You can start the installation script using this command :

'''
ansible-playbook nvme_configure.yml -i hosts.yml --ask-become-pass -vvv -e "{options}"
'''

with these available options :
- force: false, allow to remove existing raid
- only_reset: false
- auto_reboot: false
- skip_mounting: false
- skip_format: false
- skip_trimming: false

## Notes

- Need to add a waitting time/check ?

### Examples

<nvme_drive> : /dev/nvme0n1

<nvme_partition> : /dev/nvme0n1p1

<mount_folder> : /mnt/980pro1TB

<raid_folder> : /dev/md0

## Resetting NVMe Drives

- Listing Actives Raids
```
cat /proc/mdstat
```

- Unmount existings Raids
```
umount -v <mount_folder>
```

- Deleting mount folders
```
rmdir <mount_folder>
```

- Stopping existings Raids
```
mdadm --stop <raid_folder>
```

- Removing Superblocks (signature in drive)
```
mdadm --zero-superblock <nvme_drive>
```
-----------------------------------------------------
### Not mandatory actions depending of setup choices

- Removing Raids 
```
mdadm --remove <raid_folder>
```

- Removing mount point in /etc/fstab (if registered)
```
nano /etc/fstab
```

- Removing array configuration if exist
```
rm /etc/mdadm/mdadm.conf
```
------------------------------------------------------

- Checking Raids removal
```
lsblk -l | grep raid
```

- Remove signature of partition to clean raid signature
```
wipefs -a <nvme_partition> 
```

- Removing Partitions of drives
```
(echo d; echo w) | fdisk <nvme_drive> 
```

- Remove signature to clean the ext4/gpt signature store in driver and remove superblock if failed before
```
wipefs -a <nvme_drive> 
```
- Format drive completly to free still used space
```
nvme format -s1 -f <nvme_drive> 
```

- Enabling auto trimming
```
systemctl start fstrim.timer
```

## Installing NVMe Drives

- Partitionning drives (create new GPT table, new primary partition full size, Accept to rewrite partition signature if existing, save and quit)
```
(echo g; echo n; echo p; echo 1; echo ""; echo ""; echo w) | fdisk <nvme_drive> 
```

- Installing RAID
```
(echo y) | mdadm --create <raid_folder> --level=0  --raid-devices=4 <nvme_drive> <nvme_drive> ...
```
Installation will continue in background. Very fast if level=0.

- Daemonise the volume
```
mdadm --monitor --daemonise <raid_folder>
```

- Check infos
```
mdadm --detail <raid_folder>
```
```
cat /proc/mdstat
```

- Formatting partitions. Can take a lot of time. (To complete and find good parameters)
```
mkfs.ext4 -E lazy_itable_init=0 <raid_folder>
```
options: -b block-size(1024,2048,4096) -E stride=stride-size,stripe-width=stripe-width,resize=max-online-resize
see https://linux.die.net/man/8/mkfs.ext4

- Create mouting point folder
```
mkdir <mount_folder>
```

- Changing access right and group
```
chgrp -R dtdi-user <mount_folder>
```
```
chmod -R g+w <mount_folder>
```

---------------------------------------------------------

- Declare volume in fstab to mount it on startup
```
cat /etc/fstab >> <raid_folder> 	<mount_folder>	ext4	defaults 	0	1
```

- Mount all drives in fsatb
```
mount -a
```
-----------------------------------------------------------
### OR
-----------------------------------------------------------
- Mounting drives manually
```
mount -t ext4 <raid_folder> <mount_folder>
```

-----------------------------------------------------------

- Check is drive is mounted
```
df -h
```

- Disabling auto trimming (default timer once a week. queued trim can be available for some devices which allow the disk to wait minimal use before trimming)
```
systemctl disable fstrim.timer
```
```
systemctl stop fstrim.timer
```

- Check if auto trimming is enabled
```
systemctl status fstrim.timer
```

- Sending trim request (EXT3 AND NTFS-3G (auto only) NOT SUPPORTING TRIMMING)
```
fstrim <mount_folder>
```

- reboot
```
reboot
```
