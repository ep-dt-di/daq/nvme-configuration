import numpy as np
import os
import matplotlib.pyplot as plt
import math

data = {}

lines_to_save = (85, 80, 84, 139, 148, 157, 166, 147, 156, 165, 174)

import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def replaceAll(infilepath, outfilepath):
    with open(infilepath) as infile,  open(outfilepath, 'w') as outfile:
        for line in infile:
            outfile.write(line.replace('%', ''))
            
for filename in sorted(os.listdir("/home/leoj/resultsfio/"), key=natural_keys):
    f = os.path.join("/home/leoj/resultsfio/", filename)
    
    # checking if it is a file
    if os.path.isfile(f) and f.endswith('.fio'):
        
        print("Reading "+filename+" dataset")
        tmp = os.path.join("/home/leoj/resultsfio/", filename+".tmp")
        replaceAll(f, tmp)
        
        
        # Load dataset
        df = np.loadtxt(tmp, dtype='str', delimiter=';', usecols=lines_to_save)
        
        if not filename.split('-')[0] in data:
            data[filename.split('-')[0]] = {'x': [], 'Bw': [], 'latency': [], 'usage': {}}
            data[filename.split('-')[0]]['usage']['RAID'] = []
            data[filename.split('-')[0]]['usage'][df[3]] = []
            data[filename.split('-')[0]]['usage'][df[4]] = []
            data[filename.split('-')[0]]['usage'][df[5]] = []
            data[filename.split('-')[0]]['usage'][df[6]] = []
            
        
        data[filename.split('-')[0]]['usage']['RAID'].append(float(df[2]))
        data[filename.split('-')[0]]['usage'][df[3]].append(float(df[7]))
        data[filename.split('-')[0]]['usage'][df[4]].append(float(df[8]))
        data[filename.split('-')[0]]['usage'][df[5]].append(float(df[9]))
        data[filename.split('-')[0]]['usage'][df[6]].append(float(df[10]))

        data[filename.split('-')[0]]['x'].append(float(filename.split('-')[1]))
        data[filename.split('-')[0]]['Bw'].append(float(df[0]))
        data[filename.split('-')[0]]['latency'].append(float(df[1]))
        
        os.remove(tmp)

for device in data:
    
    fig, axs = plt.subplots(2, figsize=(10,13))
    ax2 = axs[0].twinx()
    
    data[device]['latency'] = [i/100 for i in data[device]['latency']]
    data[device]['Bw'] = [(i/1024)*8 for i in data[device]['Bw']]
    data[device]['x'] = [i/1024 for i in data[device]['x']]
    
    axs[0].plot(data[device]['x'], data[device]['Bw'], 'bo-')
    ax2.plot(data[device]['x'], data[device]['latency'], 'ro-')
    axs[0].tick_params(axis='x', labelrotation=45, )
    axs[0].grid(True)

    axs[0].set_xlabel('block size MiB')
    axs[0].set_xticks([data[device]['x'][i] for i in range(0, len(data[device]['x']), 5)])
    axs[0].set_yscale('log', subs=[2, 3, 4, 5, 6, 7, 8, 9])
        
    axs[0].set_ylabel('Bw Mib/s')
    ax2.set_ylabel('latency usec/s')
    
    ax2.spines['right'].set_color('red')
    ax2.spines['left'].set_color('blue')
    axs[0].tick_params(axis='y', color='blue', labelcolor='blue')
    ax2.tick_params(axis='y', color='red', labelcolor='red')
    
    axs[0].set_title(device+' Benchmark performances Sequential write, direct and sync')
    # plt.savefig("/home/leoj/resultsfio/_"+device+'_resume'+'.png')
    # plt.close()
    
    
    
    # fig, axs = plt.subplots(figsize=(12,8))
    
    for k in data[device]['usage']:
        axs[1].plot(data[device]['x'], data[device]['usage'][k], '-', label=k)
        
    axs[1].set_xlabel('block size')
    axs[1].set_ylabel('Use of disk %')
    axs[1].tick_params(axis='x', labelrotation=45)
    axs[1].set_xticks([data[device]['x'][i] for i in range(0, len(data[device]['x']), 5)])
    axs[1].grid(True)
    axs[1].legend()
    
    axs[1].set_title(device+' devices use comparaison')
    plt.savefig("/home/leoj/resultsfio/_"+device+'_use'+'.png')
    plt.close()
    
    

    