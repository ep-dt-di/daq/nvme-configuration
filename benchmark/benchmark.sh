#!/bin/sh

# block_sizes="2M 4M 6M 8M 10M 12M 14M 16M 18M 20M"
# block_sizes="2M 4M 6M 8M 10M 12M 14M 16M 18M 20M 22M 24M 26M 28M 30M"
block_sizes="1M 2M 4M 8M 16M 32M 64M 128M 256M 512M 1024M"
devices="md0 md1"
test_time=100

mkdir results

# Drop caches
sync
echo 3 > /proc/sys/vm/drop_caches

for block_size in $block_sizes; do
    echo "> Block size: $block_size"
    for device in $devices; do
        echo "Starting tests for $device"
        OUTPUT_FILE="results/$device-$block_size-results.csv"
        # Empty log file
        : > $OUTPUT_FILE

        # Start stat every 1s
        date +"%T.%N"
        iostat -x -m -p /dev/$device 1 $(($test_time+1)) >> $OUTPUT_FILE.tmp &

        # Start stat every 1s after 0.5s to have average every 0.5s
        sleep 0.5
        date +"%T.%N"
        iostat -x -m -p /dev/$device 1 $test_time >> $OUTPUT_FILE.tmp &

        sleep 0.5

        START_TIME=$(date +%s)
        while true; do
            if pgrep -x "dd" > /dev/null
            then # Running
                SPEND_TIME=$(($(date +%s) - $START_TIME))
                if [ $SPEND_TIME -ge $test_time ]; then
                    break
                fi
                sleep 0.1
                echo -ne "> [$SPEND_TIME/$test_time]\033[0K\r"
            else # Stopped
                dd if=/dev/zero of=/dev/$device bs=$block_size oflag=direct,dsync &
                echo "> *"
            fi
        done


        cat $OUTPUT_FILE.tmp | grep md | sed "s/\s\{1,\}/,/g" > $OUTPUT_FILE
        rm $OUTPUT_FILE.tmp

        echo "Finished test !"
        pkill -P $$
        killall dd
    done
done

# Re-enable cache
echo 0 > /proc/sys/vm/drop_caches

echo "Finished !"