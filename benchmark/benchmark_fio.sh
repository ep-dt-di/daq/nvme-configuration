#!/bin/sh

block_sizes=512
number_of_tests=100
devices=("md0" "md1")
cpu_affinity=("0-7,32-39" "24-31,56-63")

# Drop caches
sync
echo 3 > /proc/sys/vm/drop_caches

runtime=100

mkdir resultsfio

device_num=0
for device in ${devices[@]}; do
    echo "Starting tests for $device"
    for i in {10..15}; do
        blocksize=$(($i * $block_sizes))
        echo "> Block size: $blocksize"

        CPUALLOWED=${cpu_affinity[$device_num]} DEVICE=$device BLOCKSIZE=${blocksize}K RUNTIME=$runtime fio --output-format=terse --output=resultsfio/$device-$blocksize-results.fio job.fio &
        for pid in $(pgrep fio); do
            taskset --all-tasks -cp ${cpu_affinity[$device_num]} ${pid}
        done
        wait $!

        echo "Finished test !"
    done
    device_num=$(($device_num + 1))
done

echo "Finished !"